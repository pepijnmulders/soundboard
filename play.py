from mutagen.mp3 import MP3
from urllib import request
import soco
import time
import os

# Todo find by name instead of hardcoded IP
sonos = soco.SoCo('192.168.178.82')

PUSH_SUCCESS = 'push_success'
PUSH_FAILED = 'push_failed'
HELPDESK = 'helpdesk'

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_URL = 'https://s3.eu-central-1.amazonaws.com/tranzer-soundboard'

INIT_VOLUME = sonos.volume

commands = {
    PUSH_SUCCESS: f'{BASE_URL}/{PUSH_SUCCESS}.mp3',
    PUSH_FAILED: f'{BASE_URL}/{PUSH_FAILED}.mp3',
    HELPDESK: f'{BASE_URL}/{HELPDESK}.mp3',
}

current_track_info = sonos.get_current_track_info()

command = input()

file = request.urlretrieve(commands[command], f'{BASE_DIR}/tmp/{command}.mp3')
mp3 = MP3(file[0])

sonos.volume = 30
sonos.play_uri(commands[command])

time.sleep(mp3.info.length)

if current_track_info:
    sonos.volume = INIT_VOLUME
    sonos.play_uri(current_track_info.get('uri'))
    sonos.seek(current_track_info.get('position'))
